package control;

import model.Square;
import model.Piece;
import model.Square.SquareEventListener;

import java.awt.Color;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.awt.Point;

import view.DisplayCheckmate;

public class SquareControl implements SquareEventListener {

	public static final int ROW_NUMBER = 8;
	public static final int COL_NUMBER = 8;

	public static final Color DEFAULT_COLOR_ONE = Color.WHITE;
	public static final Color DEFAULT_COLOR_TWO = Color.GRAY;
	public static final Color DEFAULT_COLOR_HOVER = Color.BLUE;
	public static final Color DEFAULT_COLOR_SELECTED = Color.GREEN;
	public static final Color DEFAULT_COLOR_MOVED = Color.red;

	public static final Square EMPTY_SQUARE = null;

	private Color colorOne;
	private Color colorTwo;
	private Color colorHover;
	private Color colorSelected;
	protected Color colorMoves;

	private Square selectedSquare;
	private ArrayList<Square> squareList;
	protected ArrayList<Square> possibleSquares;

	public SquareControl() {
		this(DEFAULT_COLOR_ONE, DEFAULT_COLOR_TWO, DEFAULT_COLOR_HOVER,
				DEFAULT_COLOR_SELECTED,DEFAULT_COLOR_MOVED);
	}

	public SquareControl(Color colorOne, Color colorTwo, Color colorHover,
			Color colorSelected, Color colorMoves) {
		this.colorOne = colorOne;
		this.colorTwo = colorTwo;
		this.colorHover = colorHover;
		this.colorSelected = colorSelected;
		this.colorMoves = colorMoves;

		this.squareList = new ArrayList<>();
		this.possibleSquares = new ArrayList<>();
		
		createSquares();
	}

	public void resetColor(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.setColor(getGridColor(row, col));
	}

	@Override
	public void onHoverEvent(Square square) {
		square.setColor(this.colorHover);
	}

	@Override
	public void onSelectEvent(Square square) {
		int x = square.getPosition().x;
		int y = square.getPosition().y;
		System.out.println("(Row " + x + ", Column " + y + ")");

		if (haveSelectedCellPanel()) {
			if (!this.selectedSquare.equals(square)) {
				moveContentOfSelectedSquare(square);
			} else {
				unselectSquare(square);
			}			
		} else {
			selectSquare(square);
		}
	}

	public void onOutEvent(Square square) {
		if (this.selectedSquare != square && !possibleSquares.contains(square)) {
			resetColor(square);
		} else {
			if(this.selectedSquare == square)
				square.setColor(this.colorSelected);
			else
				square.setColor(Color.red);
		}
	}

	public Square getSquare(int row, int col) {
		return this.squareList.get((row * COL_NUMBER) + col);
	}

	public ArrayList<Square> getSquareList() {
		return this.squareList;
	}

	public Color getGridColor(int row, int col) {
		if ((row + col) % 2 != 0) {
			return this.colorOne;
		} else {
			return this.colorTwo;
		}
	}

	private void addSquare() {
		Square square = new Square();
		this.squareList.add(square);
		resetColor(square);
		resetPosition(square);
		square.setSquareEventListener(this);
	}

	private void resetPosition(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.getPosition().setLocation(row, col);
	}

	private boolean haveSelectedCellPanel() {
		return this.selectedSquare != EMPTY_SQUARE;
	}

	private void moveContentOfSelectedSquare(Square square) {
		try{	
			if(checkMovement(square)){
				if(square.havePiece()){
					
					if(this.selectedSquare.getPiece().getPieceColor() == "White"){
						if(!square.haveWhitePiece()){
							// Detectando checkmate apenas quando o rei é comido por outra peça
							if(square.getPiece().getPieceName() == "King"){
								System.out.println("Checkmate!");
								showCheckmate();
							}
							
							square.setPiece(this.selectedSquare.getPiece());
							this.selectedSquare.removePiece();
							unselectSquare(square);
						}
					}
					else{
						if(square.haveWhitePiece()){
							// Detectando checkmate apenas quando o rei é comido por outra peça							
							if(square.getPiece().getPieceName() == "King"){
								System.out.println("Checkmate!");
								showCheckmate();
							}
							
							square.setPiece(this.selectedSquare.getPiece());
							this.selectedSquare.removePiece();
							unselectSquare(square);	
						}
					}				
				}
				else{
					square.setPiece(this.selectedSquare.getPiece());
					this.selectedSquare.removePiece();
					unselectSquare(square);
				}
			
			}
			else{
				unselectSquare(square);
				throw new WrongMovementException();	
			}
		} catch(WrongMovementException e){
			System.out.println("Movimento não efetuado!");
		}		
	}

	private boolean checkMovement(Square square){
		
		for(int i=0; i<possibleSquares.size(); i++){
			if(possibleSquares.get(i).getPosition() == square.getPosition())
				return true;
		}
		
		return false;		
	}
		
	private void selectSquare(Square square) {
		if (square.havePiece()) {
			this.selectedSquare = square;
			this.selectedSquare.setColor(this.colorSelected);
			this.showMoves(this.selectedSquare);
		}
	}

	private void unselectSquare(Square square) {
		resetColor(this.selectedSquare);
		
		for(int i = 0; i < possibleSquares.size(); i++){
				resetColor(possibleSquares.get(i));
		}
		
		possibleSquares.clear();
		this.selectedSquare = EMPTY_SQUARE;				
	}

	private void createSquares() {
		for (int i = 0; i < (ROW_NUMBER * COL_NUMBER); i++) {
			addSquare();
		}
	}
	
	public static void showCheckmate() {		
		// Só mostra o checkmate no caso de o rei ser comido por outra peça		
		EventQueue.invokeLater(new Runnable() {

			public void run() {
				DisplayCheckmate umFrame = new DisplayCheckmate();				
				umFrame.setVisible(true);
			}			
		});		
	}
	
	private boolean pointOutOfBounds (ArrayList<Point> move, int index) {
		if(move.get(index).y > 7 || move.get(index).x > 7 || move.get(index).y < 0 || move.get(index).x < 0) {
			return true;
		}
		return false;
	}

	private void generatePossibleMoves(ArrayList<Point> moves, Square square){
		
		Piece piece = square.getPiece();
		
		if(piece.getPieceColor().equalsIgnoreCase("White")){
			if(piece.getPieceName().equalsIgnoreCase("Knight")){
				for(int i = 0; i < moves.size(); i++){
					
					Square showSquares = getSquare(moves.get(i).x, moves.get(i).y);
					
					if(!showSquares.havePiece() || !showSquares.haveWhitePiece()){
						showSquares.setColor(this.colorMoves);
						possibleSquares.add(showSquares);
					}
				}
			}
			else if(piece.getPieceName().equalsIgnoreCase("Pawn")){
				for(int i = 0; i < moves.size(); i++){
					
					Square showSquares = getSquare(moves.get(i).x,moves.get(i).y);
								
					if(showSquares.havePiece()){
						break;
					}
					
					if(!showSquares.havePiece() || !showSquares.haveWhitePiece()){
						showSquares.setColor(this.colorMoves);
						possibleSquares.add(showSquares);	
					}									
				}
			}
			else{
				for(int i = 0; i < moves.size(); i++){
					Square showSquares = getSquare(moves.get(i).x, moves.get(i).y);
					
					if(showSquares.havePiece() && !showSquares.haveWhitePiece()){
						showSquares.setColor(this.colorMoves);
						possibleSquares.add(showSquares);
						break;
					}
					if(showSquares.havePiece() && showSquares.haveWhitePiece()){
						break;
					}			
					if(!showSquares.havePiece() || !showSquares.haveWhitePiece()){
						showSquares.setColor(this.colorMoves);
						possibleSquares.add(showSquares);
					}
				}
			}
		}
		else {
			if(piece.getPieceName().equalsIgnoreCase("Knight")){
				for(int i = 0; i < moves.size(); i++){
					
					Square showSquares = getSquare(moves.get(i).x, moves.get(i).y);
					
					if(!showSquares.havePiece() || showSquares.haveWhitePiece()){
						showSquares.setColor(this.colorMoves);
						possibleSquares.add(showSquares);
					}
				}
			}
			else if(piece.getPieceName().equalsIgnoreCase("Pawn")){
				for(int i = 0; i < moves.size(); i++){
					
					Square showSquares = getSquare(moves.get(i).x, moves.get(i).y);
					
					if(showSquares.havePiece()){
						break;
					}			
					if(!showSquares.havePiece() || showSquares.haveWhitePiece()){
						showSquares.setColor(this.colorMoves);
						possibleSquares.add(showSquares);
					}						
				}
			}
			else{
				for(int i = 0; i < moves.size(); i++){
					
					Square showSquares = getSquare(moves.get(i).x, moves.get(i).y);
			
					if(showSquares.havePiece() && showSquares.haveWhitePiece()){
						showSquares.setColor(this.colorMoves);
						possibleSquares.add(showSquares);
						break;
					}
					if(showSquares.havePiece() && !showSquares.haveWhitePiece()){
						break;
					}			
					if(!showSquares.havePiece() || showSquares.haveWhitePiece()){
						showSquares.setColor(this.colorMoves);
						possibleSquares.add(showSquares);
					}
				}
			}
		}
		
	}
	
	private void generatePossibleMovesForPawnDiagonal(ArrayList<Point> moves, Square square){
		
		Piece piece = square.getPiece();
		
		if(piece.getPieceColor().equalsIgnoreCase("White")){
			for(int i = 0; i < moves.size(); i++){
				
				Square showSquares = getSquare(moves.get(i).x,moves.get(i).y);
						
				if(showSquares.havePiece() && !showSquares.haveWhitePiece()){
					showSquares.setColor(this.colorMoves);
					possibleSquares.add(showSquares);
				}
			}
		}
		else{
			for(int i = 0; i < moves.size(); i++){
				
				Square showSquares = getSquare(moves.get(i).x,moves.get(i).y);
						
				if(showSquares.havePiece() && showSquares.haveWhitePiece()){
					showSquares.setColor(this.colorMoves);
					possibleSquares.add(showSquares);
				}
			}
		}
	}
	
	// Guardar os movimentos possíveis do bispo
	private void showBishopMoves(Square square){
		
		ArrayList<Point> bishopMoves = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		
		generatePossibleMoves(bishopMoves,square);
		
		ArrayList<Point> bishopMoves1 = square.getPiece().getMoves1(square.getPosition().x, square.getPosition().y);
		
		generatePossibleMoves(bishopMoves1,square);
		
		ArrayList<Point> bishopMoves2 = square.getPiece().getMoves2(square.getPosition().x, square.getPosition().y);
		
		generatePossibleMoves(bishopMoves2,square);
		
		ArrayList<Point> bishopMoves3 = square.getPiece().getMoves3(square.getPosition().x, square.getPosition().y);
		
		generatePossibleMoves(bishopMoves3,square);	
	}
	
	// Guardar os movimentos possíveis do peão
	private void showPawnMoves(Square square){
		
		Piece piece = square.getPiece();
					
		if(piece.getPieceColor().equalsIgnoreCase("White")){
			ArrayList<Point> pawnMoves = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
			
			generatePossibleMoves(pawnMoves, square);
					
			ArrayList<Point> pawnMoves1 = square.getPiece().getMoves2(square.getPosition().x, square.getPosition().y);
			
			generatePossibleMovesForPawnDiagonal(pawnMoves1, square);
			
			ArrayList<Point> pawnMoves2 = square.getPiece().getMoves5(square.getPosition().x, square.getPosition().y);
			
			generatePossibleMovesForPawnDiagonal(pawnMoves2, square);	
		}
		else{
			ArrayList<Point> pawnMoves = square.getPiece().getMoves1(square.getPosition().x, square.getPosition().y);
			
			generatePossibleMoves(pawnMoves, square);
					
			ArrayList<Point> pawnMoves1 = square.getPiece().getMoves3(square.getPosition().x, square.getPosition().y);
			
			generatePossibleMovesForPawnDiagonal(pawnMoves1, square);
			
			ArrayList<Point> pawnMoves2 = square.getPiece().getMoves4(square.getPosition().x, square.getPosition().y);
			
			generatePossibleMovesForPawnDiagonal(pawnMoves2, square);
		}		
	}
	
	// Guardar os movimentos possíveis do Rei
	
	// Guardar os movimentos possíveis do rei	
	private void showKingMoves(Square square){
		
		ArrayList<Point> kingMoves = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		
		for(int i = 0; i < kingMoves.size(); i++){
			
			if(pointOutOfBounds(kingMoves, i)){
				continue;
			}
			
			Square showSquares = getSquare(kingMoves.get(i).x, kingMoves.get(i).y);
				
			if(!square.haveWhitePiece()){
				if(showSquares.havePiece() && showSquares.haveWhitePiece()){
					showSquares.setColor(this.colorMoves);
					possibleSquares.add(showSquares);
				}			
				else if(!showSquares.havePiece()){
					showSquares.setColor(this.colorMoves);
					possibleSquares.add(showSquares);
				}
			}
			else if (square.haveWhitePiece()){
				if(showSquares.havePiece() && !showSquares.haveWhitePiece()){
					showSquares.setColor(this.colorMoves);
					possibleSquares.add(showSquares);
				}			
				else if(!showSquares.havePiece()){
					showSquares.setColor(this.colorMoves);
					possibleSquares.add(showSquares);
				}
			}
		}		
	}
	
	// Guardar os movimentos possíveis da Torre
	
	// Guardar os movimentos possíveis da torre
	private void showRookMoves(Square square){
		
		ArrayList<Point> rookMoves = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		
		generatePossibleMoves(rookMoves,square);
		
		ArrayList<Point> rookMoves1 = square.getPiece().getMoves1(square.getPosition().x, square.getPosition().y);
		
		generatePossibleMoves(rookMoves1,square);
		
		ArrayList<Point> rookMoves2 = square.getPiece().getMoves2(square.getPosition().x, square.getPosition().y);
		
		generatePossibleMoves(rookMoves2,square);
		
		ArrayList<Point> rookMoves3 = square.getPiece().getMoves3(square.getPosition().x, square.getPosition().y);
		
		generatePossibleMoves(rookMoves3,square);
	}	
	
	// Guardar os movimentos possíveis da Dama
	
	// Guardar os movimentos possíveis da dama	
	private void showQueenMoves(Square square){
		
		ArrayList<Point> queenMoves = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		
		generatePossibleMoves(queenMoves,square);
		
		ArrayList<Point> queenMoves1 = square.getPiece().getMoves1(square.getPosition().x, square.getPosition().y);
		
		generatePossibleMoves(queenMoves1,square);
		
		ArrayList<Point> queenMoves2 = square.getPiece().getMoves2(square.getPosition().x, square.getPosition().y);
		
		generatePossibleMoves(queenMoves2,square);
		
		ArrayList<Point> queenMoves3 = square.getPiece().getMoves3(square.getPosition().x, square.getPosition().y);
		
		generatePossibleMoves(queenMoves3,square);
		
		ArrayList<Point> queenMoves4 = square.getPiece().getMoves4(square.getPosition().x, square.getPosition().y);
		
		generatePossibleMoves(queenMoves4,square);
		
		ArrayList<Point> queenMoves5 = square.getPiece().getMoves5(square.getPosition().x, square.getPosition().y);
		
		generatePossibleMoves(queenMoves5,square);
		
		ArrayList<Point> queenMoves6 = square.getPiece().getMoves6(square.getPosition().x, square.getPosition().y);
		
		generatePossibleMoves(queenMoves6,square);
		
		ArrayList<Point> queenMoves7 = square.getPiece().getMoves7(square.getPosition().x, square.getPosition().y);
		
		generatePossibleMoves(queenMoves7,square);	
		
	}
	
	// Guardar os movimentos possíveis do Cavalo
	
	// Guardar os movimentos possíveis do cavalo
	private void showKnightMoves(Square square){
		
		ArrayList<Point> knightMoves = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
				
		generatePossibleMoves(knightMoves, square);
	}
	
	// Mostrar os movimentos disponíveis de todas as peças
	private void showMoves(Square square){
		
		Piece piece = square.getPiece();
		
		if(piece.getPieceName().equalsIgnoreCase("King"))
			showKingMoves(square);
		
		if(piece.getPieceName().equalsIgnoreCase("Bishop"))
			showBishopMoves(square);
		
		if(piece.getPieceName().equalsIgnoreCase("Pawn"))
			showPawnMoves(square);
		
		if(piece.getPieceName().equalsIgnoreCase("Rook"))
			showRookMoves(square);
		
		if(piece.getPieceName().equalsIgnoreCase("Queen"))
			showQueenMoves(square);
		
		if(piece.getPieceName().equalsIgnoreCase("Knight"))
			showKnightMoves(square);
	}
}
