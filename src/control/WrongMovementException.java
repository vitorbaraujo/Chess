package control;

public class WrongMovementException extends Exception {

	private static final long serialVersionUID = 1234;

	public WrongMovementException() {
		System.out.print("Impossível mover para esta casa. ");
	}	
	
}
