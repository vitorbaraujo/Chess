package view;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

// Mostra se há chequemate apenas quando o rei é comido por outra peça

public class DisplayCheckmate extends JFrame {

	private Dimension dimension;
	private JPanel jPanel;
	private JLabel jLabel;
	
	private static final long serialVersionUID = -451215;
	
	public DisplayCheckmate(){
		this.dimension = new Dimension(100,50);		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);		
				
		this.jPanel = new JPanel();
		this.jPanel.setPreferredSize(this.dimension);
				
		this.jLabel = new JLabel("Checkmate!");
		
		this.jPanel.add(this.jLabel);
				
		add(this.jPanel);
		pack();
		
		setLocationRelativeTo(null);
	}
	
}
