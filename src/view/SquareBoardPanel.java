package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import model.*;

import javax.swing.JPanel;

import control.SquareControl;

public class SquareBoardPanel extends JPanel {

	private static final long serialVersionUID = 7332850110063699836L;

	private SquareControl squareControl;
	private ArrayList<SquarePanel> squarePanelList;

	public SquareBoardPanel() {
		setLayout(new GridBagLayout());
		this.squarePanelList = new ArrayList<SquarePanel>();

		initializeSquareControl();
		initializeGrid();
		initializePiecesInChess();
	}

	private void initializeSquareControl() {
		Color colorOne = Color.WHITE;
		Color colorTwo = Color.GRAY;
		Color colorHover = Color.BLUE;
		Color colorSelected = Color.GREEN;
		Color moveColor = Color.red;

		this.squareControl = new SquareControl(colorOne, colorTwo, colorHover,
				colorSelected, moveColor);
	}

	private void initializeGrid() {
		GridBagConstraints gridBag = new GridBagConstraints();

		Square square;
		for (int i = 0; i < this.squareControl.getSquareList().size(); i++) {
			square = this.squareControl.getSquareList().get(i);
			gridBag.gridx = square.getPosition().y;
			gridBag.gridy = square.getPosition().x;
//			square.getPosition().setLocation(gridBag.gridx, gridBag.gridy);

			SquarePanel squarePanel = new SquarePanel(square);

			add(squarePanel, gridBag);
			this.squarePanelList.add(squarePanel);
		}

	}

	private void initializePiecesInChess() {
		Piece brownPawn = new Pawn("Pawn","icon/Brown P_48x48.png","Brown");
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(1, i).setPiece(brownPawn);
		}

		Piece brownRook = new Rook("Rook","icon/Brown R_48x48.png","Brown");
		this.squareControl.getSquare(0, 0).setPiece(brownRook);
		this.squareControl.getSquare(0, 7).setPiece(brownRook);

		Piece brownKnight = new Knight("Knight","icon/Brown N_48x48.png","Brown"); 
		this.squareControl.getSquare(0, 1).setPiece(brownKnight);
		this.squareControl.getSquare(0, 6).setPiece(brownKnight);

		Piece brownBishop = new Bishop("Bishop","icon/Brown B_48x48.png","Brown");
		this.squareControl.getSquare(0, 2).setPiece(brownBishop);
		this.squareControl.getSquare(0, 5).setPiece(brownBishop);

		Piece brownQueen = new Queen("Queen","icon/Brown Q_48x48.png","Brown"); 
		this.squareControl.getSquare(0, 3).setPiece(brownQueen);

		Piece brownKing = new King("King","icon/Brown K_48x48.png","Brown");
		this.squareControl.getSquare(0, 4).setPiece(brownKing);
		
		Piece whitePawn = new Pawn("Pawn","icon/White P_48x48.png","White");
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(6, i).setPiece(whitePawn);
		}
		
		Piece whiteRook = new Rook("Rook","icon/White R_48x48.png","White");
		this.squareControl.getSquare(7, 0).setPiece(whiteRook);
		this.squareControl.getSquare(7, 7).setPiece(whiteRook);

		Piece whiteKnight = new Knight("Knight","icon/White N_48x48.png","White");
		this.squareControl.getSquare(7, 1).setPiece(whiteKnight);
		this.squareControl.getSquare(7, 6).setPiece(whiteKnight);

		Piece whiteBishop = new Bishop("Bishop","icon/White B_48x48.png","White");
		this.squareControl.getSquare(7, 2).setPiece(whiteBishop);
		this.squareControl.getSquare(7, 5).setPiece(whiteBishop);

		Piece whiteQueen = new Queen("Queen","icon/White Q_48x48.png","White");
		this.squareControl.getSquare(7, 3).setPiece(whiteQueen);

		Piece whiteKing = new King("King","icon/White K_48x48.png","White");
		this.squareControl.getSquare(7, 4).setPiece(whiteKing);
	}
}
