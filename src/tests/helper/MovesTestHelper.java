package tests.helper;

import java.awt.Point;
import java.util.ArrayList;

public class MovesTestHelper {

	public static final int POSITION_X = 4;
	public static final int POSITION_Y = 4;
	
	public static ArrayList<Point> getWhitePawnMoves(int x, int y){
		ArrayList<Point> moves = new ArrayList<Point>();

		if(x!=0){
			moves.add(new Point(x - 1,y));
							
			if (x == 6)			
				moves.add(new Point(x - 2,y));
		}
	
		return moves;
	}
	
	public static ArrayList<Point> getBrownPawnMoves(int x, int y){
		ArrayList<Point> moves = new ArrayList<Point>();

		if(x!=7){
			moves.add(new Point(x + 1,y));
				
			if (x == 1)			
				moves.add(new Point(x + 2,y));
		}
				
		return moves;
	}
	
	public static ArrayList<Point> getUpTowerMoves(int i,int j) {
		ArrayList<Point> moves = new ArrayList<Point>();
		
		for(int x=i;x>0;x--) {
			if(x==0) break;

			Point rookPoint = new Point(x-1, j);
			moves.add(rookPoint);		
		}
		
		return moves;
	}
	
	public static ArrayList<Point> getDownTowerMoves(int i,int j) {
		ArrayList<Point> moves = new ArrayList<Point>();
		
		for(int x=i; x<8;x++) {
			if(x==7) break;
			
			Point rookPoint = new Point(x+1,j);
			moves.add(rookPoint);			
		}
		
		return moves;
	}
	
	public static ArrayList<Point> getLeftTowerMoves(int i,int j) {
		ArrayList<Point> moves = new ArrayList<Point>();
		
		for(int x=j;x>0;x--) {
			if(x==0) break;

			Point rookPoint = new Point(i, x-1);
			moves.add(rookPoint);			
		}
		
		return moves;
	}
	
	public static ArrayList<Point> getRightTowerMoves(int i,int j) {
		ArrayList<Point> moves = new ArrayList<Point>();
		
		for(int x=j;x<8;x++) {
			if(x==7) break;
			
			Point rookPoint = new Point(i, x+1);
			moves.add(rookPoint);			
		}
		
		return moves;
	}
	
	public static ArrayList<Point> getHorseMoves() {
		int x = POSITION_X;
		int y = POSITION_Y;
		ArrayList<Point> moves = new ArrayList<Point>();

		moves.add(new Point(x - 1, y + 2));
		moves.add(new Point(x + 1, y + 2));
		moves.add(new Point(x + 1, y - 2));
		moves.add(new Point(x - 1, y - 2));
		moves.add(new Point(x - 2, y + 1));
		moves.add(new Point(x - 2, y - 1));
		moves.add(new Point(x + 2, y + 1));
		moves.add(new Point(x + 2, y - 1));

		return moves;
	}

	public static ArrayList<Point> getUpLeftDiagonalBishopMoves(int i, int j) {
		ArrayList<Point> moves = new ArrayList<Point>();
		
		for(int x = i, y = j; x>0 || y>0; x--, y--) {
			if(x==0 || y==0)
				break;

			Point bishopPoint = new Point(x-1, y-1);
			moves.add(bishopPoint);			
		}
		
		return moves;
	}
	
	public static ArrayList<Point> getUpRightDiagonalBishopMoves(int i, int j) {
		ArrayList<Point> moves = new ArrayList<Point>();
		
		for(int x=i,y=i; x>0 || y<7; x--,y++) {
			if(x==0 || y==7)
				break;

			Point bishopPoint = new Point(x-1, y+1);
			moves.add(bishopPoint);		
		}
		
		return moves;
	}
	
	public static ArrayList<Point> getDownLeftDiagonalBishopMoves(int i, int j) {
		ArrayList<Point> moves = new ArrayList<Point>();
		
		for(int x=i,y=j; x<7 || y>0; x++,y--) {
			if(x==7 || y==0)
				break;
			
			Point bishopPoint = new Point(x+1, y-1);
			moves.add(bishopPoint);			
		}
		
		return moves;
	}
	
	public static ArrayList<Point> getDownRightDiagonalBishopMoves(int i, int j) {
		ArrayList<Point> moves = new ArrayList<Point>();
		
		for(int x=i,y=i; x<7 || y<7 ; x++,y++) {
			if(x==7 || y==7)
				break;
			
			Point bishopPoint = new Point(x+1, y+1);
			moves.add(bishopPoint);
		}
		
		return moves;
	}
	
	public static ArrayList<Point> getKingMoves() {
		int x = POSITION_X;
		int y = POSITION_Y;
		ArrayList<Point> moves = new ArrayList<Point>();

		moves.add(new Point(x + 1, y + 1));
		moves.add(new Point(x + 0, y + 1));
		moves.add(new Point(x - 1, y + 1));
		moves.add(new Point(x - 1, y + 0));
		moves.add(new Point(x - 1, y - 1));
		moves.add(new Point(x + 0, y - 1));
		moves.add(new Point(x + 1, y - 1));
		moves.add(new Point(x + 1, y + 0));

		return moves;
	}
}
