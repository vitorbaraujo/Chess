package tests.model;

import java.awt.Point;
import java.util.ArrayList;

import model.Piece;
import model.Piece.Team;
import model.*;

import org.junit.Assert;
import org.junit.Test;

import tests.helper.MovesTestHelper;

public class PieceTest {

	private static final Class<?> CLASSE_PAWN = Pawn.class;
	private static final Class<?> CLASSE_ROOK = Rook.class;
	private static final Class<?> CLASSE_CAVALO = Knight.class;
	private static final Class<?> CLASSE_BISPO = Bishop.class;
	private static final Class<?> CLASSE_REI = King.class;
	private static final Class<?> CLASSE_RAINHA = Queen.class;
	
	@Test
	public void testMovePawn() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;

		Piece pieceUpTeam = newPieceInstance(CLASSE_PAWN, Team.UP_TEAM);
		Piece pieceDownTeam = newPieceInstance(CLASSE_PAWN, Team.DOWN_TEAM);

		assertMoves(MovesTestHelper.getWhitePawnMoves(x, y), pieceUpTeam.getMoves(x, y));
		assertMoves(MovesTestHelper.getBrownPawnMoves(x, y), pieceDownTeam.getMoves1(x, y));
		
	}

	@Test
	public void testMoveTower() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;

		Piece piece = newPieceInstance(CLASSE_ROOK, Team.UP_TEAM);

//		assertMoves(MovesTestHelper.getTowerMoves(), piece.getMoves(x, y));		
		assertMoves(MovesTestHelper.getUpTowerMoves(x,y), piece.getMoves2(x, y));
		assertMoves(MovesTestHelper.getDownTowerMoves(x,y), piece.getMoves(x, y));
		assertMoves(MovesTestHelper.getLeftTowerMoves(x,y), piece.getMoves3(x, y));
		assertMoves(MovesTestHelper.getRightTowerMoves(x,y), piece.getMoves1(x, y));
	}

	@Test
	public void testMoveHorse() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;

		Piece piece = newPieceInstance(CLASSE_CAVALO, Team.UP_TEAM);

		assertMoves(MovesTestHelper.getHorseMoves(), piece.getMoves(x, y));
	}

	@Test
	public void testMoveBishop() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;

		Piece piece = newPieceInstance(CLASSE_BISPO, Team.UP_TEAM);

		assertMoves(MovesTestHelper.getUpLeftDiagonalBishopMoves(x,y), piece.getMoves3(x, y));
		assertMoves(MovesTestHelper.getUpRightDiagonalBishopMoves(x,y), piece.getMoves2(x, y));
		assertMoves(MovesTestHelper.getDownLeftDiagonalBishopMoves(x,y), piece.getMoves1(x, y));
		assertMoves(MovesTestHelper.getDownRightDiagonalBishopMoves(x,y), piece.getMoves(x, y));
	}

	@Test
	public void testMoveKing() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;

		Piece piece = newPieceInstance(CLASSE_REI, Team.UP_TEAM);

		assertMoves(MovesTestHelper.getKingMoves(), piece.getMoves(x, y));
	}

	@Test
	public void testMoveQueen() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;

		Piece piece = newPieceInstance(CLASSE_RAINHA, Team.UP_TEAM);
		
		assertMoves(MovesTestHelper.getUpTowerMoves(x,y),piece.getMoves1(x, y));
		assertMoves(MovesTestHelper.getDownTowerMoves(x,y),piece.getMoves(x, y));
		assertMoves(MovesTestHelper.getLeftTowerMoves(x,y),piece.getMoves3(x, y));
		assertMoves(MovesTestHelper.getRightTowerMoves(x,y),piece.getMoves2(x, y));
		assertMoves(MovesTestHelper.getUpLeftDiagonalBishopMoves(x,y),piece.getMoves7(x, y));
		assertMoves(MovesTestHelper.getUpRightDiagonalBishopMoves(x,y),piece.getMoves6(x, y));
		assertMoves(MovesTestHelper.getDownLeftDiagonalBishopMoves(x,y),piece.getMoves5(x, y));
		assertMoves(MovesTestHelper.getDownRightDiagonalBishopMoves(x,y),piece.getMoves4(x, y));		
	}

	private Piece newPieceInstance(Class<?> classPiece, Team team)
			throws Exception {
		return (Piece) classPiece.getDeclaredConstructor(Team.class)
				.newInstance(team);
	}

	private void assertMoves(ArrayList<Point> movesA, ArrayList<Point> movesB)
			throws Exception {
		if (movesA.size() != movesB.size()) {
			Assert.assertTrue(false);
			return;
		}

		for (Point point : movesA) {
			if (!movesB.contains(point)) {
				Assert.assertTrue(false);
				return;
			}
		}

		Assert.assertTrue(true);
	}
}
