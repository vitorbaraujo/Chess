package model;

import java.util.ArrayList;
import java.awt.Point;
import javax.swing.text.Position;

public abstract class Piece {
	private String pieceName;
	private String piecePath;
	private String pieceColor;
	private ArrayList<Position> moves;
	
	public static final int CHESSBOARD_ROW = 8;
	public static final int CHESSBOARD_COL = 8;

	private Team team;
	
	public Piece(String pieceName, String piecePath, String pieceColor) {
		 super();
		 this.pieceName = pieceName;
		 this.piecePath = piecePath;
		 this.pieceColor = pieceColor;
	}	

	public String getPieceName() {
		return pieceName;
	}

	public void setPieceName(String pieceName) {
		this.pieceName = pieceName;
	}

	public String getPiecePath() {
		return piecePath;
	}

	public String getPieceColor() {
		return pieceColor;
	}

	public void setPieceColor(String pieceColor) {
		this.pieceColor = pieceColor;
	}

	public ArrayList<Position> getMoves() {
		return moves;
	}

	public void setMoves(ArrayList<Position> moves) {
		this.moves = moves;
	}

	public void setPiecePath(String piecePath) {
		this.piecePath = piecePath;
	}
	
	public ArrayList<Point> getMoves(int x, int y) {
		return null;
	}
	
	public ArrayList<Point> getMoves1(int x, int y) {
		return null;
	}
	
	public ArrayList<Point> getMoves2(int x, int y) {
		return null;
	}
	
	public ArrayList<Point> getMoves3(int x, int y) {
		return null;
	}
	
	public ArrayList<Point> getMoves4(int x, int y) {
		return null;
	}
	
	public ArrayList<Point> getMoves5(int x, int y) {
		return null;
	}
	
	public ArrayList<Point> getMoves6(int x, int y) {
		return null;
	}
	
	public ArrayList<Point> getMoves7(int x, int y) {
		return null;
	}
	
	// Métodos dos testes automatizados
	
	public static enum Team {
		UP_TEAM, DOWN_TEAM;
	}

	public Piece(Team team) {
		this.team = team;
	}

	public boolean pieceIsOnMyTeam(Piece piece) {
		return this.team == piece.getTeam();
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}
}