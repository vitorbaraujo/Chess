package model;

import java.awt.Point;
import java.util.ArrayList;

public class Pawn extends Piece {

	public Pawn(String pieceName, String piecePath, String pieceColor) {
		super(pieceName, piecePath,pieceColor);
	}	
	
	public Pawn(Team team) {
		super(team);
	}

	//inicio movimentacao normal peão branco	
	@Override
	public ArrayList<Point> getMoves(int x, int y) {
	
		ArrayList<Point> moves = new ArrayList<Point>();

		if(x!=0){
			moves.add(new Point(x - 1,y));
							
			if (x == 6)			
				moves.add(new Point(x - 2,y));
		}
	
		return moves;
	}	
	//fim movimentacao normal peão branco
	
	//inicio movimentacao normal peão marrom	
	@Override
	public ArrayList<Point> getMoves1(int x, int y) {
	
		ArrayList<Point> moves = new ArrayList<Point>();

		if(x!=7){
			moves.add(new Point(x + 1,y));
				
			if (x == 1)			
				moves.add(new Point(x + 2,y));
		}
				
		return moves;
	}	
	//fim movimentacao normal peão marrom
	
	//Diagonal acima à esquerda (para peão branco)
	@Override
	public ArrayList<Point> getMoves2(int row,int col) {
		ArrayList<Point> pawnMoves = new ArrayList<>();
		
		if(row-1 >= 0 && col-1 >=0){
				Point diagUL = new Point(row-1,col-1);
				pawnMoves.add(diagUL);
		}			
		
		return pawnMoves;
	}

	//Diagonal abaixo à direita (para peão marrom)
	@Override
	public ArrayList<Point> getMoves3(int row,int col) {
		ArrayList<Point> pawnMoves = new ArrayList<>();
		
		if(row+1 <= 7 && col+1 <=7){
				Point diagDR = new Point(row+1,col+1);
				pawnMoves.add(diagDR);
		}
		
		return pawnMoves;
	}
	
	//Diagonal abaixo à esquerda (para peão marrom)
	@Override
	public ArrayList<Point> getMoves4(int row,int col) {
		ArrayList<Point> pawnMoves = new ArrayList<>();
		
		if(row+1 <= 7 && col-1 >=0){
				Point diagUL = new Point(row+1,col-1);
				pawnMoves.add(diagUL);
		}
		
		return pawnMoves;
	}
	
	//Diagonal acima à direita (para peão marrom)
	@Override
	public ArrayList<Point> getMoves5(int row,int col) {
		ArrayList<Point> pawnMoves = new ArrayList<>();
		
		if(row-1 >= 0 && col+1 <=7){
			Point diagUR = new Point(row-1,col+1);
			pawnMoves.add(diagUR);
		}
		
		return pawnMoves;
	}
}
