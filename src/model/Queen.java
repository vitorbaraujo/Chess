package model;

import java.awt.Point;
import java.util.ArrayList;

public class Queen extends Piece {

	public Queen(String pieceName, String piecePath, String pieceColor) {
		super(pieceName, piecePath, pieceColor);
	}
	
	public Queen(Team team) {
		super(team);
	}

	// Coluna abaixo
	@Override
	public ArrayList<Point> getMoves(int row, int col) {

		ArrayList<Point> queenMoves = new ArrayList<>();

		for (int x = row; x < 7; x++) {
			if (x == 7)
				break;
		
			Point queenPoint = new Point(x + 1, col);
			queenMoves.add(queenPoint);
		}

		return queenMoves;
	}
	
	// Coluna acima
	@Override
	public ArrayList<Point> getMoves1(int row,int col) {
		ArrayList<Point> queenMoves = new ArrayList<>();
		
		for (int x = row; x > 0; x--) {
			if (x == 0)
				break;

			Point queenPoint = new Point(x - 1, col);
			queenMoves.add(queenPoint);

		}
		
		return queenMoves;		
	}
	
	// Linha à direita
	@Override
	public ArrayList<Point> getMoves2(int row,int col) {
		ArrayList<Point> queenMoves = new ArrayList<>();
		
		for (int x = col; x < 7; x++) {
			if (x == 7)
				break;

			Point queenPoint = new Point(row, x + 1);
			queenMoves.add(queenPoint);
		}
		
		return queenMoves;		
	}
	
	// Linha à esquerda
	@Override
	public ArrayList<Point> getMoves3(int row,int col) {
		ArrayList<Point> queenMoves = new ArrayList<>();
		
		for (int x = col; x > 0; x--) {
			if (x == 0)
				break;

			Point queenPoint = new Point(row, x - 1);
			queenMoves.add(queenPoint);
		}
		
		return queenMoves;		
	}
	
	// Diagonal abaixo à direita
	@Override
	public ArrayList<Point> getMoves4(int row,int col) {
		ArrayList<Point> queenMoves = new ArrayList<>();
		
		for (int x = row, y = col; x < 7 || y < 7; x++, y++) {
			if (x == 7 || y == 7)
				break;

			Point queenPoint = new Point(x + 1, y + 1);
			queenMoves.add(queenPoint);
		}
		
		return queenMoves;		
	}
	
	// Diagonal abaixo à esquerda
	@Override
	public ArrayList<Point> getMoves5(int row,int col) {
		ArrayList<Point> queenMoves = new ArrayList<>();
		
		for (int x = row, y = col; x < 7 || y > 0; x++, y--) {
			if (x == 7 || y == 0)
				break;

			Point queenPoint = new Point(x + 1, y - 1);
			queenMoves.add(queenPoint);
		}
		
		return queenMoves;		
	}
	
	// Diagonal acima à direita
	@Override
	public ArrayList<Point> getMoves6(int row,int col) {
		ArrayList<Point> queenMoves = new ArrayList<>();
		
		for (int x = row, y = col; x > 0 || y < 7; x--, y++) {
			if (x == 0 || y == 7)
				break;

			Point queenPoint = new Point(x - 1, y + 1);
			queenMoves.add(queenPoint);
		}
		
		return queenMoves;		
	}
	
	// Diagonal acima à esquerda
	@Override
	public ArrayList<Point> getMoves7(int row,int col) {
		ArrayList<Point> queenMoves = new ArrayList<>();
		
		for (int x = row, y = col; x > 0 || y > 0; x--, y--) {
			if (x == 0 || y == 0)
				break;

			Point queenPoint = new Point(x - 1, y - 1);
			queenMoves.add(queenPoint);
		}
		
		return queenMoves;		
	}
}
