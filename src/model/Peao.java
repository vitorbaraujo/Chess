package model;

import java.awt.Point;
import java.util.ArrayList;

public class Peao extends Piece {

	private ArrayList<Point> moves;

	public Peao(Team team) {
		super(team);
		this.moves = new ArrayList<Point>();
	}

	@Override
	public ArrayList<Point> getMoves(int x, int y) {
		this.moves.clear();

		Point point;
		if (getTeam() == Team.UP_TEAM) {
			point = new Point(x, y - 1);
		} else {
			point = new Point(x, y + 1);
		}

		this.moves.add(point);

		return this.moves;
	}
}
