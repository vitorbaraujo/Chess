package model;

import java.awt.Point;
import java.util.ArrayList;

public class Rook extends Piece {

	public Rook(String pieceName, String piecePath, String pieceColor) {
		super(pieceName, piecePath,pieceColor);
	}	
	
	public Rook(Team team) {
		super(team);
	}

	// Coluna abaixo
	@Override
	public ArrayList<Point> getMoves(int row, int col) {
		
		ArrayList<Point> rookMoves = new ArrayList<>();
		
		for(int x=row; x<8;x++) {
			if(x==7) break;
			
			Point rookPoint = new Point(x+1,col);
			rookMoves.add(rookPoint);			
		}
		
		return rookMoves;
	}
	
	// Linha à direita
	@Override
	public ArrayList<Point> getMoves1(int row, int col) {

		ArrayList<Point> rookMoves = new ArrayList<>();
		
		for(int x=col;x<8;x++) {
			if(x==7) break;
			
			Point rookPoint = new Point(row, x+1);
			rookMoves.add(rookPoint);			
		}
		
		return rookMoves;		
	}
	
	// Coluna acima
	@Override
	public ArrayList<Point> getMoves2(int row, int col) {

		ArrayList<Point> rookMoves = new ArrayList<>();
		
		for(int x=row;x>0;x--) {
			if(x==0) break;

			Point rookPoint = new Point(x-1, col);
			rookMoves.add(rookPoint);		
		}
		
		return rookMoves;		
	}
	
	// Linha à esquerda
	@Override
	public ArrayList<Point> getMoves3(int row, int col) {

		ArrayList<Point> rookMoves = new ArrayList<>();
		
		for(int x=col;x>0;x--) {
			if(x==0) break;

			Point rookPoint = new Point(row, x-1);
			rookMoves.add(rookPoint);			
		}
		
		return rookMoves;		
	}
}
