package model;

import java.util.ArrayList;
import java.awt.Point;

public class Bishop extends Piece {

	public Bishop(String pieceName, String piecePath, String pieceColor) {
		super(pieceName, piecePath,pieceColor);
	}
	
	public Bishop(Team team) {
		super(team);		
	}

	public ArrayList<Point> getMoves(int row,int col){
		ArrayList<Point> bishopMoves = new ArrayList<>();
		
		for(int x=row,y=col; x<7 || y<7 ; x++,y++) {
			if(x==7 || y==7)
				break;
			
			Point bishopPoint = new Point(x+1, y+1);
			bishopMoves.add(bishopPoint);
		}
		
		return bishopMoves;		
	}
	
	public ArrayList<Point> getMoves1(int row,int col){
		
		ArrayList<Point> bishopMoves = new ArrayList<>();
		
		for(int x=row,y=col; x<7 || y>0; x++,y--) {
			if(x==7 || y==0)
				break;
			
			Point bishopPoint = new Point(x+1, y-1);
			bishopMoves.add(bishopPoint);			
		}
		
		return bishopMoves;
		
	}
	
	public ArrayList<Point> getMoves2(int row,int col){
		
		ArrayList<Point> bishopMoves = new ArrayList<>();
		
		for(int x=row,y=col; x>0 || y<7; x--,y++) {
			if(x==0 || y==7)
				break;

			Point bishopPoint = new Point(x-1, y+1);
			bishopMoves.add(bishopPoint);		
		}
		
		return bishopMoves;		
	}
	
	public ArrayList<Point> getMoves3(int row,int col){
		
		ArrayList<Point> bishopMoves = new ArrayList<>();
		
		for(int x = row, y = col; x>0 || col>0; x--, y--) {
			if(x==0 || y==0)
				break;

			Point bishopPoint = new Point(x-1, y-1);
			bishopMoves.add(bishopPoint);			
		}
		
		return bishopMoves;		
	}	
}
