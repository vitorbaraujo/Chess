package model;

import java.awt.Point;
import java.util.ArrayList;

public class Knight extends Piece {

	public Knight(String pieceName, String piecePath, String pieceColor) {
		super(pieceName, piecePath,pieceColor);
	}
	
	public Knight(Team team) {
		super(team);
	}

	@Override
	public ArrayList<Point> getMoves(int row, int col) {
	
		ArrayList<Point> knightMoves = new ArrayList<>();
		
		for(int x=row, y=col; x<row+1 || y<col+1; x++,y++){
			if(x==7 || y==7) break;
			
			if(y+1<=7 && x+2<=7){
				Point knightPoint = new Point(x+2, y+1);
				knightMoves.add(knightPoint);	
			}
			if(y+2<=7 && x+1<=7){
				Point knightPoint2 = new Point(x+1, y+2);
				knightMoves.add(knightPoint2);
			}
		}
		for(int x=row, y=col; x<row+1 || y>col-1; x++,y--){
			if(x==7 || y==0) break;
			
			if(y-1>=0 && x+2<=7){
				Point knightPoint = new Point(x+2, y-1);
				knightMoves.add(knightPoint);	
			}
			if(y-2>=0 && x+1<=7){
				Point knightPoint2 = new Point(x+1, y-2);
				knightMoves.add(knightPoint2);
			}
		}
		for(int x=row, y=col; x>row-1 || y<col+1; x--,y++){
			if(x==0 || y==7) break;
			
			if(y+1<=7 && x-2>=0){
				Point knightPoint = new Point(x-2, y+1);
				knightMoves.add(knightPoint);	
			}
			if(y+2<=7 && x-1>=0){
				Point knightPoint2 = new Point(x-1, y+2);
				knightMoves.add(knightPoint2);
			}
		}
		for(int x=row, y=col; x>row-1 || y>col-1; x--,y--){
			if(x==0 || y==0) break;
			
			if(y-1>=0 && x-2>=0){
				Point knightPoint = new Point(x-2, y-1);
				knightMoves.add(knightPoint);	
			}
			if(y-2>=0 && x-1>=0){
				Point knightPoint2 = new Point(x-1, y-2);
				knightMoves.add(knightPoint2);
			}
		}		
		
		return knightMoves;
		
	}	
}
